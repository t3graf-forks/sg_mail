<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "sg_mail".
 ***************************************************************/

$EM_CONF['sg_mail'] = [
	'title' => 'Mail Templates',
	'description' => 'Mail Templates',
	'category' => 'module',
	'version' => '8.1.0',
	'state' => 'stable',
	'uploadfolder' => FALSE,
	'createDirs' => '',
	'clearcacheonload' => FALSE,
	'author' => 'Fabian Galinski',
	'author_email' => 'fabian@sgalinski.de',
	'author_company' => 'sgalinski Internet Services (https://www.sgalinski.de)',
	'constraints' => [
		'depends' => [
			'typo3' => '10.4.0-11.5.99',
			'php' => '7.3.0-8.1.99',
		],
		'conflicts' => [
		],
		'suggests' => [
		],
	],
];
