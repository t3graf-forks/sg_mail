<?php
/******************************************************************************
 * Copyright notice                                                           *
 *                                                                            *
 * (c) sgalinski Internet Services (https://www.sgalinski.de)                 *
 *                                                                            *
 * All rights reserved                                                        *
 *                                                                            *
 * This script is part of the TYPO3 project. The TYPO3 project is             *
 * free software; you can redistribute it and/or modify                       *
 * it under the terms of the GNU General Public License as published by       *
 * the Free Software Foundation; either version 3 of the License, or          *
 * (at your option) any later version.                                        *
 *                                                                            *
 * The GNU General Public License can be found at                             *
 * http://www.gnu.org/copyleft/gpl.html.                                      *
 *                                                                            *
 * This script is distributed in the hope that it will be useful,             *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 * GNU General Public License for more details.                               *
 *                                                                            *
 * This copyright notice MUST APPEAR in all copies of the script!             *
 ******************************************************************************/

namespace SGalinski\SgMail\Controller;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\Request;

/**
 * Class SiteController
 *
 * @package SGalinski\SgMail\Controller
 */
class SiteController extends ActionController {
	/**
	 * Action to select a site out of the available sites
	 */
	public function indexAction(): ?\Psr\Http\Message\ResponseInterface {
		$out = [];
		$sites = GeneralUtility::makeInstance(SiteFinder::class)->getAllSites();
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
			->getQueryBuilderForTable('tx_sgmail_domain_model_mail');
		$queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));
		$rows = $queryBuilder->select('pid')
			->from('tx_sgmail_domain_model_mail')
			->groupBy('pid')
			->execute()->fetchAll();
		$siteIds = array_map(static function ($site) {
			return $site->getRootPageId();
		}, $sites);
		$rowIds = array_column($rows, 'pid');
		$pids = array_unique(array_merge($siteIds, $rowIds));
		foreach ($pids as $pid) {
			$pageInfo = BackendUtility::readPageAccess($pid, $GLOBALS['BE_USER']->getPagePermsClause(1));
			if ($pageInfo) {
				$rootline = BackendUtility::BEgetRootLine($pageInfo['uid'], '', TRUE);
				ksort($rootline);
				$path = '/root';
				foreach ($rootline as $page) {
					$path .= '/p' . dechex($page['uid']);
				}
				$pageInfo['path'] = $path;
				$out[] = $pageInfo;
			}
		}

		if (version_compare(\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version(), '11.0.0', '<')) {
			$this->view->assign('V11', FALSE);
			$controllerName = $this->request->getOriginalRequest()->getControllerName();
			$actionName = $this->request->getOriginalRequest()->getControllerActionName();

			$this->view->assignMultiple([
				'sites' => $out,
				'controller' => $controllerName,
				'action' => $actionName
			]);
			return NULL;
		} else {
			$this->view->assign('V11', TRUE);
			/** @var Request $originalRequest */
			$originalRequest = $this->request->getArgument('originalRequest');

			$controllerName = $originalRequest->getAttribute('extbase')->getControllerName();
			$actionName = $originalRequest->getAttribute('extbase')->getControllerActionName();

			$this->view->assignMultiple([
				'sites' => $out,
				'controller' => $controllerName,
				'action' => $actionName
			]);
			return $this->createBackendResponse();
		}
	}

	/**
	 * Use the ModuleTemplateResponse to create a response object for the backend
	 *
	 * @return  \Psr\Http\Message\ResponseInterface
	 */
	protected function createBackendResponse(): \Psr\Http\Message\ResponseInterface {
		$moduleTemplateFactory = GeneralUtility::makeInstance(\TYPO3\CMS\Backend\Template\ModuleTemplateFactory::class);
		$moduleTemplate = $moduleTemplateFactory->create($this->request);
		$moduleTemplate->setContent($this->view->render());
		return $this->htmlResponse($moduleTemplate->renderContent());
	}
}
