<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Controller;

use SGalinski\SgMail\Domain\Model\Layout;
use SGalinski\SgMail\Domain\Model\Template;
use SGalinski\SgMail\Domain\Repository\LayoutRepository;
use SGalinski\SgMail\Domain\Repository\TemplateRepository;
use SGalinski\SgMail\Service\MailTemplateService;
use TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Resource\Exception\ResourceDoesNotExistException;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use TYPO3\CMS\Extbase\Http\ForwardResponse;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException;
use TYPO3\CMS\Extbase\Mvc\Exception\StopActionException;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;
use TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;
use TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * Controller for the mail templating service module
 */
class MailController extends AbstractController {
	/**
	 * @var TemplateRepository
	 */
	protected $templateRepository;

	/**
	 * Inject the TemplateRepository
	 *
	 * @param TemplateRepository $templateRepository
	 */
	public function injectTemplateRepository(TemplateRepository $templateRepository): void {
		$this->templateRepository = $templateRepository;
	}

	/**
	 * @var LayoutRepository
	 */
	protected $layoutRepository;

	/**
	 * Inject the LayoutRepository
	 *
	 * @param LayoutRepository $layoutRepository
	 */
	public function injectLayoutRepository(LayoutRepository $layoutRepository): void {
		$this->layoutRepository = $layoutRepository;
	}

	/**
	 * @param ViewInterface $view
	 */
	public function initializeView(ViewInterface $view): void {
		parent::initializeView($view);
		$view->assign('controller', 'Mail');
	}

	/**
	 * Show template Selection and enable content input + mail preview
	 *
	 * @param array $parameters
	 * @return \Psr\Http\Message\ResponseInterface|null
	 * @throws InvalidQueryException
	 * @throws NoSuchArgumentException
	 * @throws NoSuchCacheException
	 * @throws SiteNotFoundException
	 * @throws StopActionException
	 * @throws \SGalinski\SgMail\Exceptions\TemplateNotFoundException
	 */
	public function indexAction(array $parameters = []): ?\Psr\Http\Message\ResponseInterface {
		if (empty($parameters)) {
			$parameters = $this->request->getArguments();
		}
		$this->switchMode();
		$forwardMissingSite = $this->requireSite();
		if ($forwardMissingSite !== NULL) {
			return $forwardMissingSite;
		}
		$this->addMessage();

		$registerArray = $this->registerService->getNonBlacklistedTemplates($this->site->getRootPageId());
		// if no extensions are registered, redirect to empty action
		if (!\is_array($registerArray) || \count($registerArray) <= 0) {
			if (version_compare(VersionNumberUtility::getCurrentTypo3Version(), '11.0.0', '<')) {
				$this->forward('empty');
				return NULL;
			}
			return (new ForwardResponse('empty'));
		}
		if (
			!isset($parameters['selectedTemplate'])
			|| $parameters['selectedTemplate'] === ''
		) {
			[$parameters['selectedExtension'], $parameters['selectedTemplate']] = $this->getSelectedExtensionAndTemplate(
			);
		}

		// store selected template & extension key in the session
		$this->writeToSession('selectedTemplate', $parameters['selectedTemplate']);
		$this->writeToSession('selectedExtension', $parameters['selectedExtension']);
		$site = GeneralUtility::makeInstance(SiteFinder::class)->getSiteByPageId($this->site->getRootPageId());
		$languages = $site->getLanguages();
		$templatesFromDb = $this->templateRepository->findByTemplateProperties(
			$parameters['selectedExtension'],
			$parameters['selectedTemplate'],
			$languages,
			$this->site->getRootPageId()
		)->toArray();

		// if no templates are in the db, get the default from the files
		$templates = [];
		foreach ($languages as $language) {
			$template = NULL;
			foreach ($templatesFromDb as $_template) {
				/** @var Template $_template */
				if ($_template->getSiteLanguage() === $language) {
					$template = $_template;
					break;
				}
			}

			if ($template === NULL) {
				$templateFromFile = $this->registerService->findTemplate(
					$parameters['selectedExtension'],
					$parameters['selectedTemplate'],
					$language
				);
				$template = GeneralUtility::makeInstance(Template::class);
				$template->setSiteLanguage($language);
				$templateFromFile['pid'] = $this->site->getRootPageId();
				$this->templateRepository->fillTemplate(
					$template,
					$templateFromFile
				);
			} else {
				$template->setIsOverwritten(TRUE);
			}

			$templates[$language->getLanguageId()] = $template;
		}

		// calculating optimal column width for the view
		$colspace = 4;
		$templateCount = \count($templates);
		if ($templateCount % 2 === 0 && $templateCount <= 4) {
			$colspace = 6;
		} elseif ($templateCount === 1) {
			$colspace = 12;
		}

		$this->view->assignMultiple([
			'colspace' => $colspace,
			'templates' => $templates,
			'register' => $registerArray,
			'isManual' => $this->registerService->isManuallyRegisteredTemplate($parameters['selectedTemplate'])
		]);

		$templateDescription = $registerArray[$parameters['selectedExtension']][$parameters['selectedTemplate']]['description'];
		if (\is_array($templateDescription)) {
			$templateDescription = $templateDescription[$site->getDefaultLanguage()->getTwoLetterIsoCode()];
		} elseif (\strpos($templateDescription, 'LLL:') !== FALSE) {
			$templateDescription = LocalizationUtility::translate(
				$templateDescription,
				$parameters['selectedExtension']
			);
		}

		$layouts = $this->layoutRepository->findByPidForModule($this->site->getRootPageId());
		$layoutOptions = [
			Layout::DEFAULT_LAYOUT => LocalizationUtility::translate('backend.layout.default', 'SgMail'),
			Layout::NO_LAYOUT => LocalizationUtility::translate('backend.layout.none', 'SgMail')
		];
		foreach ($layouts as $layout) {
			$layoutOptions[(int) $layout['uid']] = $layout['name'];
		}

		$selectedTemplate = $registerArray[$parameters['selectedExtension']][$parameters['selectedTemplate']];
		if (!isset($selectedTemplate['marker']['page'])) {
			$selectedTemplate['marker']['page']['marker'] = 'page';
			$selectedTemplate['marker']['page']['type'] = 'Array';
			$selectedTemplate['marker']['page']['value'] = LocalizationUtility::translate(
				'backend.marker.page.value',
				$this->request->getControllerExtensionName()
			);
			$selectedTemplate['marker']['page']['description'] = LocalizationUtility::translate(
				'backend.marker.page.description',
				$this->request->getControllerExtensionName()
			);
		}

		$this->view->assignMultiple([
			'templateDescription' => $templateDescription,
			'selectedTemplate' => $selectedTemplate,
			'selectedTemplateKey' => $parameters['selectedTemplate'],
			'selectedExtensionKey' => $parameters['selectedExtension'],
			'layoutOptions' => $layoutOptions
		]);

		$this->makeDocheader();
		$this->view->assignMultiple([
			'beUserMail' => $GLOBALS['BE_USER']->user['email']
		]);

		if (version_compare(VersionNumberUtility::getCurrentTypo3Version(), '11.0.0', '<')) {
			return NULL;
		} else {
			$this->view->assign('V11', TRUE);
			return $this->createBackendResponse();
		}
	}

	/**
	 * send a test email to a given address
	 * redirect to index action
	 *
	 * @param array $parameters
	 * @throws IllegalObjectTypeException
	 * @throws NoSuchCacheException
	 * @throws ResourceDoesNotExistException
	 * @throws SiteNotFoundException
	 * @throws StopActionException
	 * @throws UnknownObjectException
	 * @throws \TYPO3\CMS\Extbase\Object\Exception
	 */
	public function sendTestMailAction(array $parameters = []): ?\Psr\Http\Message\ResponseInterface {
		$arguments = [];
		if ($parameters['selectedExtension']) {
			$arguments['selectedExtension'] = $parameters['selectedExtension'];
		}

		if ($parameters['selectedTemplate']) {
			$arguments['selectedTemplate'] = $parameters['selectedTemplate'];
		}

		foreach ((array) $parameters['templates'] as $parameter) {
			$addresses = array_merge(
				GeneralUtility::trimExplode(',', $parameter['bcc']),
				GeneralUtility::trimExplode(',', $parameter['cc'])
			);
			if (!$this->checkMailAddresses($addresses)) {
				if (version_compare(VersionNumberUtility::getCurrentTypo3Version(), '11.0.0', '<')) {
					$this->redirect('index', NULL, NULL, $arguments);
					return NULL;
				} else {
					return $this->redirect('index', NULL, NULL, $arguments);
				}
			}
		}

		$forwardMissingSite = $this->requireSite();
		if ($forwardMissingSite !== NULL) {
			return $forwardMissingSite;
		}
		$templates = [];
		foreach ((array) $parameters['templates'] as $template) {
			if ($template['uid']) {
				$templates[] = $this->templateRepository->updateByUid($template['uid'], $template);
			} else {
				$templates[] = $this->templateRepository->create($template);
			}
		}

		$message = LocalizationUtility::translate('backend.success', 'sg_mail');
		$this->addFlashMessage($message);

		if (!$this->request->hasArgument('saveOnly')) {
			if (!filter_var($parameters['emailAddress'], FILTER_VALIDATE_EMAIL)) {
				if (version_compare(VersionNumberUtility::getCurrentTypo3Version(), '11.0.0', '<')) {
					$this->redirect('index', NULL, NULL, $arguments);
					return NULL;
				}

				return $this->redirect('index', NULL, NULL, $arguments);
			}

			$mailTemplateService = GeneralUtility::makeInstance(
				MailTemplateService::class,
				$parameters['selectedTemplate'],
				$parameters['selectedExtension']
			);
			$mailTemplateService->setIgnoreMailQueue(TRUE);

			$mailIsSend = FALSE;
			foreach ($templates as $template) {
				$mailTemplateService->loadTemplateValues($template);
				$mailTemplateService->setToAddresses($parameters['emailAddress']);
				$mailTemplateService->setPreviewMarkers();
				$mailTemplateService->setSiteLanguage($template->getSiteLanguage());
				$mailIsSend = $mailTemplateService->sendEmail();
			}

			if ($mailIsSend) {
				$message = LocalizationUtility::translate('backend.success_mail', 'sg_mail');
				$this->addFlashMessage($message);
			} else {
				$message = LocalizationUtility::translate('backend.failure_mail', 'sg_mail');
				$this->addFlashMessage($message, '', FlashMessage::ERROR);
			}
		}

		if (version_compare(VersionNumberUtility::getCurrentTypo3Version(), '11.0.0', '<')) {
			$this->redirect('index', NULL, NULL, $arguments);
			return NULL;
		}

		return $this->redirect('index', NULL, NULL, $arguments);
	}

	/**
	 * show a notice when no extension is registered
	 */
	public function emptyAction(): ?\Psr\Http\Message\ResponseInterface {
		// Nothing to do here, the fluid template will handle it
		if (version_compare(VersionNumberUtility::getCurrentTypo3Version(), '11.0.0', '<')) {
			return NULL;
		}

		return $this->createBackendResponse();
	}

	/**
	 * reset template values to default state for a specific language (delete from db)
	 *
	 * @param string $template
	 * @param string $extensionKey
	 * @throws StopActionException
	 */
	public function resetAction(string $template, string $extensionKey): ?\Psr\Http\Message\ResponseInterface {
		$forwardMissingSite = $this->requireSite();
		if ($forwardMissingSite !== NULL) {
			return $forwardMissingSite;
		}
		$this->templateRepository->deleteTemplate($extensionKey, $template, $this->site->getRootPageId());

		$message = LocalizationUtility::translate('backend.template_reset', 'sg_mail');
		$this->addFlashMessage($message, '', FlashMessage::OK);

		$arguments = $this->request->getArguments();

		if (version_compare(VersionNumberUtility::getCurrentTypo3Version(), '11.0.0', '<')) {
			$this->redirect('index', NULL, NULL, $arguments);
			return NULL;
		} else {
			return $this->redirect('index', NULL, NULL, $arguments);
		}
	}
}
