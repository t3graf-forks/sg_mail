<?php
/*******************************************************************************
 * Copyright notice
 *
 * (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ******************************************************************************/

namespace SGalinski\SgMail\Controller;

use SGalinski\SgMail\Service\RegisterService;
use TYPO3\CMS\Backend\Template\Components\ButtonBar;
use TYPO3\CMS\Backend\Template\Components\DocHeaderComponent;
use TYPO3\CMS\Backend\Template\ModuleTemplate;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException;
use TYPO3\CMS\Core\Exception\SiteNotFoundException;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Site\Entity\Site;
use TYPO3\CMS\Core\Site\SiteFinder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Http\ForwardResponse;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException;
use TYPO3\CMS\Extbase\Mvc\Exception\StopActionException;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use function count;

/**
 * Class AbstractController
 *
 * @package SGalinski\SgMail\Controller
 */
abstract class AbstractController extends ActionController {
	public const sessionKey = 'sg_mail_controller_session';
	public const DEFAULT_EXTENSION_KEY = 'sg_mail';

	/**
	 * @var BackendUserAuthentication
	 */
	protected $backendUserAuthentication;

	/**
	 * @param BackendUserAuthentication $backendUserAuthentication
	 */
	public function injectBackendUserAuthentication(BackendUserAuthentication $backendUserAuthentication): void {
		$this->backendUserAuthentication = $GLOBALS['BE_USER'];
	}

	/**
	 * @var ?ModuleTemplate
	 */
	protected $moduleTemplate = NULL;

	/**
	 * @var RegisterService
	 */
	protected $registerService;

	/**
	 * @var Site
	 */
	protected $site;

	/**
	 * @param RegisterService $registerService
	 */
	public function injectRegisterService(RegisterService $registerService): void {
		$this->registerService = $registerService;
	}

	/**
	 * Get data from the session
	 *
	 * @param string $key
	 * @return string
	 */
	protected function getFromSession(string $key): string {
		return $this->backendUserAuthentication->getSessionData(self::sessionKey . '_' . $key) ?? '';
	}

	/**
	 * Write data to the session
	 *
	 * @param string $key
	 * @param mixed $data
	 */
	protected function writeToSession(string $key, $data): void {
		$this->backendUserAuthentication->setAndSaveSessionData(self::sessionKey . '_' . $key, $data);
	}

	/**
	 * Get the mode from session and switch to it if necessary
	 *
	 * @throws NoSuchArgumentException
	 * @throws StopActionException
	 */
	protected function switchMode(): void {
		$mode = $this->getFromSession('mode');
		if ($this->request->hasArgument('controller')) {
			$mode = $this->request->getArgument('controller');
		}

		if ($mode) {
			$this->writeToSession('mode', $mode);
			if ('SGalinski\\SgMail\\Controller\\' . $mode . 'Controller' !== static::class) {
				$this->redirect('index', $this->getFromSession('mode'));
			}
		}
	}

	/**
	 * Check if a pid has been set and redirect if there is none
	 *
	 * @throws StopActionException
	 */
	protected function requireSite(): ?\Psr\Http\Message\ResponseInterface {
		$pid = $this->getPid();
		if (!$pid) {
			$originalRequest = clone $this->request;
			$this->request->setOriginalRequest($originalRequest);
			if (version_compare(
				\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version(),
				'11.0.0',
				'<'
			)) {
				$this->forward('index', 'Site', 'SgMail');
				return NULL;
			}
			return (new ForwardResponse('index'))
				->withControllerName('Site')
				->withExtensionName('SgMail')
				->withArguments(['originalRequest' => $originalRequest]);
		}

		try {
			$this->site = GeneralUtility::makeInstance(SiteFinder::class)->getSiteByPageId($pid);
		} catch (SiteNotFoundException $exception) {
			$originalRequest = clone $this->request;
			$this->request->setOriginalRequest($originalRequest);
			if (version_compare(
				\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version(),
				'11.0.0',
				'<'
			)) {
				$this->forward('index', 'Site', $this->request->getArguments());
				return NULL;
			}
			return (new ForwardResponse('index'))
				->withControllerName('Site')
				->withExtensionName('SgMail')
				->withArguments($this->request->getArguments());
		}
		return NULL;
	}

	/**
	 * Add the message from the request arguments as a flash message
	 *
	 * @throws NoSuchArgumentException
	 */
	protected function addMessage(): void {
		if ($this->request->hasArgument('message')) {
			$this->addFlashMessage($this->request->getArgument('message'), '', FlashMessage::INFO);
		}
	}

	/**
	 * Get the page id from get parameters
	 *
	 * @return int
	 */
	protected function getPid(): int {
		return (int) GeneralUtility::_GP('id');
	}

	/**
	 * Get the selected extension and template from the register
	 *
	 * @return array
	 * @throws NoSuchCacheException
	 */
	protected function getSelectedExtensionAndTemplateFromRegister(): array {
		$pageUid = $this->getPid();
		$registerArray = $this->registerService->getNonBlacklistedTemplates($pageUid);
		$selectedExtension = '';
		$selectedTemplate = '';
		if (!empty($registerArray)) {
			$selectedExtension = key($registerArray);
		}

		if (!empty($registerArray[$selectedExtension])) {
			$selectedTemplate = key($registerArray[$selectedExtension]);
		}

		return [$selectedExtension, $selectedTemplate];
	}

	/**
	 * Get the selected extension and template from session or register if session is empty
	 *
	 * @return array
	 * @throws NoSuchCacheException
	 */
	protected function getSelectedExtensionAndTemplate(): array {
		if (
			$this->getFromSession('selectedTemplate') !== NULL &&
			$this->getFromSession('selectedExtension') !== NULL &&
			!$this->registerService->isTemplateBlacklisted(
				$this->getFromSession('selectedExtension'),
				$this->getFromSession('selectedTemplate'),
				$this->getPid()
			)
		) {
			return [$this->getFromSession('selectedExtension'), $this->getFromSession('selectedTemplate')];
		}

		return $this->getSelectedExtensionAndTemplateFromRegister();
	}

	/**
	 * This function checks an array of mail addresses on validity and returns false if one of them is invalid
	 *
	 * @param array $addresses
	 * @return bool
	 */
	protected function checkMailAddresses(array $addresses): bool {
		if (count($addresses) > 0) {
			foreach ($addresses as $address) {
				if (
					trim($address) !== '' &&
					!filter_var($address, FILTER_VALIDATE_EMAIL) &&
					!(
						strpos($address, '{') !== FALSE &&
						strpos($address, '{') < strpos($address, '}')
					)
				) {
					$message = LocalizationUtility::translate('backend.error_cc', 'sg_mail');
					$this->addFlashMessage($message, '', FlashMessage::WARNING);
					return FALSE;
				}
			}
		}
		return TRUE;
	}

	/**
	 * Make the docheader component
	 */
	protected function makeDocheader(): void {
		$pageUid = $this->getPid();
		$pageInfo = BackendUtility::readPageAccess($pageUid, $GLOBALS['BE_USER']->getPagePermsClause(1));

		if (version_compare(\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version(), '11.0.0', '<')) {
			$docHeaderComponent = GeneralUtility::makeInstance(DocHeaderComponent::class);
		} else {
			$this->getModuleTemplate();
			$docHeaderComponent = $this->moduleTemplate->getDocHeaderComponent();
		}

		if ($pageInfo === FALSE) {
			$pageInfo = ['uid' => $pageUid];
		}
		$docHeaderComponent->setMetaInformation($pageInfo);
		$this->makeButtons($docHeaderComponent);

		if (version_compare(\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version(), '11.0.0', '<')) {
			$this->view->assign('docHeader', $docHeaderComponent->docHeaderContent());
		}
	}

	/**
	 * create buttons for the backend module header
	 *
	 * @param DocHeaderComponent $docHeaderComponent
	 */
	protected function makeButtons(DocHeaderComponent $docHeaderComponent): void {
		$buttonBar = $docHeaderComponent->getButtonBar();
		/** @var IconFactory $iconFactory */
		$iconFactory = GeneralUtility::makeInstance(IconFactory::class);
		$locallangPath = 'LLL:EXT:core/Resources/Private/Language/locallang_core.xlf:';
		// Refresh
		$refreshButton = $buttonBar->makeLinkButton()
			->setHref(GeneralUtility::getIndpEnv('REQUEST_URI'))
			->setTitle(
				LocalizationUtility::translate(
					$locallangPath . 'labels.reload'
				)
			)
			->setIcon($iconFactory->getIcon('actions-refresh', Icon::SIZE_SMALL));
		$buttonBar->addButton($refreshButton, ButtonBar::BUTTON_POSITION_RIGHT);
		// shortcut button
		$shortcutButton = $buttonBar->makeShortcutButton()
			->setModuleName($this->request->getPluginName())
			->setGetVariables(
				[
					'id',
					'M'
				]
			)
			->setSetVariables([]);
		$buttonBar->addButton($shortcutButton, ButtonBar::BUTTON_POSITION_RIGHT);
	}

	/**
	 * Use the ModuleTemplateResponse to create a response object for the backend
	 *
	 * @return  \Psr\Http\Message\ResponseInterface
	 */
	protected function createBackendResponse(): \Psr\Http\Message\ResponseInterface {
		return $this->htmlResponse($this->view->render());
	}

	/**
	 * Since we cannot use constructer Injection, we do have to get one Dynamicly. If we want to use
	 * our ModuleTemplate, call GetModuleTemplate before, which will init one if there is none for some reason
	 */
	protected function getModuleTemplate() {
		if ($this->moduleTemplate === NULL) {
			$moduleTemplateFactory = GeneralUtility::makeInstance(
				\TYPO3\CMS\Backend\Template\ModuleTemplateFactory::class
			);
			$this->moduleTemplate = $moduleTemplateFactory->create($this->request);
		}
	}
}
