<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Domain\Repository;

use SGalinski\SgMail\Domain\Model\Layout;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use TYPO3\CMS\Extbase\Object\ObjectManagerInterface;
use TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Repository for the Template object
 */
class LayoutRepository extends Repository {
	public const LAYOUT_TABLE_NAME = 'tx_sgmail_domain_model_layout';

	/**
	 * LayoutRepository constructor.
	 *
	 * @param ObjectManagerInterface $objectManager
	 */
	public function __construct(ObjectManagerInterface $objectManager) {
		parent::__construct($objectManager);
		$querySettings = GeneralUtility::makeInstance(Typo3QuerySettings::class);

		$querySettings->setRespectStoragePage(FALSE);
		$querySettings->setLanguageOverlayMode(TRUE);
		$querySettings->setRespectSysLanguage(FALSE);

		if (version_compare(VersionNumberUtility::getCurrentTypo3Version(), '11.0.0', '<')) {
			$querySettings->setLanguageMode('content_fallback');
		}

		$this->setDefaultQuerySettings($querySettings);
	}

	/**
	 * Fetches the layout by uid or the default by pid.
	 * Returns NULL, if "no layout" was selected.
	 *
	 * @param int $uid
	 * @param int $pid
	 * @param int $languageUid
	 * @return NULL|Layout
	 */
	public function findByUidOrDefault(int $uid, int $pid, int $languageUid = 0): ?Layout {
		if ($uid === Layout::NO_LAYOUT) {
			return NULL;
		}

		$query = $this->createQuery();
		$querySettings = $query->getQuerySettings();
		$querySettings->setLanguageUid($languageUid);
		$query->setQuerySettings($querySettings);
		$query->setLimit(1);

		if ($uid === Layout::DEFAULT_LAYOUT) {
			$result = $query->matching(
				$query->logicalAnd(
					[
						$query->equals('pid', $pid),
						$query->equals('default', 1)
					]
				)
			)->execute();
		} else {
			$result = $query->matching($query->equals('uid', $uid))->execute();
		}

		return $result->getFirst();
	}

	/**
	 * Returns the layout records for the given page uid
	 *
	 * @param int $pageUid
	 * @return array
	 */
	public function findByPidForModule(int $pageUid): array {
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(
			self::LAYOUT_TABLE_NAME
		);
		$queryBuilder->getRestrictions()->removeAll()->add(GeneralUtility::makeInstance(DeletedRestriction::class));
		return $queryBuilder->selectLiteral('l.*')->from(self::LAYOUT_TABLE_NAME, 'l')
			->where(
				$queryBuilder->expr()->andX(
					$queryBuilder->expr()->eq(
						'l.pid',
						$queryBuilder->createNamedParameter($pageUid, Connection::PARAM_INT)
					),
					$queryBuilder->expr()->in(
						'l.sys_language_uid',
						$queryBuilder->createNamedParameter([0, -1], Connection::PARAM_INT_ARRAY)
					)
				)
			)
			->orderBy('l.name')->execute()->fetchAll();
	}
}
