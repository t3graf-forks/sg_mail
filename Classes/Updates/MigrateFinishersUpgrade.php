<?php
/**
 * Copyright notice
 *
 * (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 */

namespace SGalinski\SgMail\Updates;

use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Authentication\CommandLineUserAuthentication;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Localization\LanguageService;
use TYPO3\CMS\Core\Localization\LanguageServiceFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use TYPO3\CMS\Form\Mvc\Persistence\Exception\PersistenceManagerException;
use TYPO3\CMS\Form\Mvc\Persistence\FormPersistenceManager;
use TYPO3\CMS\Install\Updates\ChattyInterface;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

/**
 * Class MigrateFinishersUpgrade
 *
 * @package SGalinski\SgMail\Updates
 */
class MigrateFinishersUpgrade implements UpgradeWizardInterface, ChattyInterface {
	/**
	 * THe wizard identifier
	 */
	public const IDENTIFIER = 'tx_sgmail_migratefinishers';

	/**
	 * @var OutputInterface
	 */
	protected $output;

	/**
	 * @var FormPersistenceManager
	 */
	protected $formPersistenceManager;

	/**
	 * @inheritDoc
	 */
	public function getIdentifier(): string {
		return self::IDENTIFIER;
	}

	/**
	 * @inheritDoc
	 */
	public function getTitle(): string {
		return 'Migrates the MailToSenderFinisher and MailToReceiverFinisher';
	}

	/**
	 * @inheritDoc
	 */
	public function getDescription(): string {
		return '';
	}

	/**
	 * @inheritDoc
	 */
	public function executeUpdate(): bool {
		$this->initializeBackendUserAuthentication();
		$this->initializeFormPersistenceManager();
		$formData = $this->loadFormData();
		foreach ($formData as $persistenceIdentifier => $_formData) {
			$formChanged = FALSE;
			if (isset($_formData['readOnly']) && $_formData['readOnly']) {
				$this->output->writeln('Form with identifier "' . $_formData['identifier'] . '" is readonly!');
				continue;
			}

			if (
				isset($_formData['finishers']) &&
				is_array($_formData['finishers'])
			) {
				foreach ($_formData['finishers'] as &$finisher) {
					if (
						$finisher['identifier'] === 'MailToSenderFinisher' ||
						$finisher['identifier'] === 'MailToReceiverFinisher'
					) {
						$finisher = $this->migrateFinisher($finisher);
						$formChanged = TRUE;
					}
				}
				unset($finisher);
			}

			if ($formChanged) {
				$this->output->writeln('Finishers of form with identifier "' . $_formData['identifier'] . '" migrated!');
				$this->writeBack($persistenceIdentifier, $_formData);
			}
		}

		return TRUE;
	}

	/**
	 * @inheritDoc
	 */
	public function updateNecessary(): bool {
		$this->initializeBackendUserAuthentication();
		$this->initializeFormPersistenceManager();
		$formData = $this->loadFormData();
		foreach ($formData as $_formData) {
			if (isset($_formData['finishers']) && is_array($_formData['finishers'])) {
				foreach ($_formData['finishers'] as $finisher) {
					if (
						$finisher['identifier'] === 'MailToSenderFinisher' ||
						$finisher['identifier'] === 'MailToReceiverFinisher'
					) {
						return TRUE;
					}
				}
			}
		}

		return FALSE;
	}

	/**
	 * Migrates the finisher to the new one
	 *
	 * @param array $finisher
	 * @return array
	 */
	protected function migrateFinisher(array $finisher): array {
		if ($finisher['identifier'] === 'MailToSenderFinisher') {
			$finisher['identifier'] = 'MailToUserFinisher';
		}
		if ($finisher['identifier'] === 'MailToReceiverFinisher') {
			$finisher['identifier'] = 'MailToAdminFinisher';
		}

		return $finisher;
	}

	/**
	 * Load and return the formData of all registered forms
	 *
	 * @return array
	 */
	protected function loadFormData(): array {
		$forms = $this->formPersistenceManager->listForms();
		$formData = [];
		foreach ($forms as $form) {
			if (!array_key_exists('invalid', $form) || (bool) $form['invalid'] === FALSE) {
				$_formData = $this->formPersistenceManager->load($form['persistenceIdentifier']);
				if (!array_key_exists('invalid', $_formData) || (bool) $_formData['invalid'] === FALSE) {
					$formData[$form['persistenceIdentifier']] = $_formData;
				}
			}
		}

		return $formData;
	}

	/**
	 * Write back the form data to the YAML file
	 *
	 * @param string $persistenceIdentifier
	 * @param array $formData
	 * @throws PersistenceManagerException
	 */
	protected function writeBack(string $persistenceIdentifier, array $formData): void {
		$this->formPersistenceManager->save($persistenceIdentifier, $formData);
	}

	/**
	 * Initialize the FormPersistenceManager
	 */
	protected function initializeFormPersistenceManager(): void {
		$this->formPersistenceManager = GeneralUtility::makeInstance(FormPersistenceManager::class);
		if (version_compare(VersionNumberUtility::getCurrentTypo3Version(), '11.0.0', '<')) {
			$this->formPersistenceManager->initializeObject();
		}
	}

	/**
	 * Initialize a BackendUserAuthentication if none exists
	 */
	protected function initializeBackendUserAuthentication(): void {
		if (!$GLOBALS['BE_USER'] instanceof BackendUserAuthentication) {
			if (Environment::isCli()) {
				$GLOBALS['BE_USER'] = GeneralUtility::makeInstance(CommandLineUserAuthentication::class);
			} else {
				$GLOBALS['BE_USER'] = GeneralUtility::makeInstance(BackendUserAuthentication::class);
				$GLOBALS['BE_USER']->start();
			}

			if (version_compare(VersionNumberUtility::getCurrentTypo3Version(), '11.0.0', '<')) {
				$GLOBALS['LANG'] = LanguageService::createFromUserPreferences($GLOBALS['BE_USER']);
			} else {
				$GLOBALS['LANG'] = GeneralUtility::makeInstance(LanguageServiceFactory::class)->createFromUserPreferences($GLOBALS['BE_USER']);
			}
		}
	}

	/**
	 * @inheritDoc
	 */
	public function getPrerequisites(): array {
		return [];
	}

	/**
	 * @param OutputInterface $output
	 */
	public function setOutput(OutputInterface $output): void {
		$this->output = $output;
	}
}
