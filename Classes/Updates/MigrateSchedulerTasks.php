<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Updates;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Updates\DatabaseUpdatedPrerequisite;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;
use TYPO3\CMS\Scheduler\Scheduler;
use TYPO3\CMS\Scheduler\Task\ExecuteSchedulableCommandTask;

/**
 * Class MigrateSchedulerTasks
 *
 * @package SGalinski\SgMail\Updates
 */
class MigrateSchedulerTasks implements UpgradeWizardInterface {
	/**
	 * Identifier of the upgrade
	 */
	public const IDENTIFIER = 'tx_sgmail_migrateschedulertasks';

	/**
	 * @inheritDoc
	 */
	public function getIdentifier(): string {
		return self::IDENTIFIER;
	}

	/**
	 * @inheritDoc
	 */
	public function getTitle(): string {
		return 'Migrate sg_mail scheduler tasks to symfony commands API';
	}

	/**
	 * @inheritDoc
	 */
	public function getDescription(): string {
		return 'This upgrade migrates all sg_mail scheduler tasks, created with the old CommandController API to the new Symfony Commands API';
	}

	/**
	 * @inheritDoc
	 */
	public function executeUpdate(): bool {
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(
			'tx_scheduler_task'
		);
		$tasks = $queryBuilder->select('serialized_task_object')
			->from('tx_scheduler_task')
			->where(
				$queryBuilder->expr()->eq('disable', 0)
			)->execute()->fetchAll();
		foreach ($tasks as $_task) {
			try {
				$task = unserialize($_task['serialized_task_object'], [\__PHP_Incomplete_Class::class]);
				$taskVars = $this->cleanArrayKeys((array) $task);
				$identifier = $taskVars['*commandIdentifier'] ?? '';
				if (
					$identifier === 'sg_mail:sendmail:runsendmails'
				) {
					$this->replaceTask($taskVars);
				}
			} catch (\Exception $exception) {
				continue;
			}
		}

		return TRUE;
	}

	/**
	 * Replace the given extbase task implementation with the new Symfony Commands API one
	 *
	 * @param array $task
	 */
	protected function replaceTask(array $task): void {
		$commandTask = GeneralUtility::makeInstance(ExecuteSchedulableCommandTask::class);
		switch ($task['*commandIdentifier']) {
			case 'sg_mail:sendmail:runsendmails':
				$commandTask->setCommandIdentifier('sg_mail:sendMail');
				break;
		}

		if (is_array($task['*arguments'])) {
			$commandTask->setArguments($task['*arguments']);
		}

		if (is_array($task['*defaults'])) {
			foreach ($task['*defaults'] as $key => $default) {
				$commandTask->addDefaultValue($key, $default);
			}
		}

		$commandTask->setTaskGroup($task['*taskGroup']);
		$commandTask->setExecution($task['*execution']);
		$commandTask->setExecutionTime($task['*executionTime']);
		$commandTask->setRunOnNextCronJob($task['*runOnNextCronJob']);
		$commandTask->setTaskUid($task['*taskUid']);
		GeneralUtility::makeInstance(Scheduler::class)->saveTask($commandTask);
	}

	/**
	 * @inheritDoc
	 */
	public function updateNecessary(): bool {
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(
			'tx_scheduler_task'
		);
		$tasks = $queryBuilder->select('serialized_task_object')
			->from('tx_scheduler_task')
			->where(
				$queryBuilder->expr()->eq('disable', 0)
			)->execute()->fetchAll();
		foreach ($tasks as $_task) {
			try {
				$task = unserialize($_task['serialized_task_object'], [\__PHP_Incomplete_Class::class]);
				$taskVars = $this->cleanArrayKeys((array) $task);
				$identifier = $taskVars['*commandIdentifier'] ?? '';
				if (
					$identifier == 'sg_mail:sendmail:runsendmails'
				) {
					$this->replaceTask($taskVars);
				}
			} catch (\Exception $exception) {
				continue;
			}
		}

		return FALSE;
	}

	/**
	 * Cleans array keys from their hidden \0
	 *
	 * @param array $array
	 * @return array
	 */
	protected function cleanArrayKeys(array $array): array {
		$newArray = [];
		foreach ($array as $key => $value) {
			$newArray[str_replace("\0", '', $key)] = $value;
		}
		return $newArray;
	}

	/**
	 * @inheritDoc
	 */
	public function getPrerequisites(): array {
		return [
			DatabaseUpdatedPrerequisite::class
		];
	}
}
