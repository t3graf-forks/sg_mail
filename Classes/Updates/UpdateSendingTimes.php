<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Updates;

use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Install\Updates\DatabaseUpdatedPrerequisite;
use TYPO3\CMS\Install\Updates\UpgradeWizardInterface;

/**
 * Update the correct sending times (if any)
 */
class UpdateSendingTimes implements UpgradeWizardInterface {
	/**
	 * The wizard identifier
	 */
	public const IDENTIFIER = 'tx_sgmail_update_sending_times';

	/**
	 * @var string
	 */
	protected $table = 'tx_sgmail_domain_model_mail';

	/**
	 * @return string
	 */
	public function getIdentifier(): string {
		return 'tx_sgmail_update_sending_times';
	}

	/**
	 * @return string
	 */
	public function getTitle(): string {
		return 'Find all sent mails and set the sending time to the tstamp field';
	}

	/**
	 * @return string
	 */
	public function getDescription(): string {
		return 'Update the sending times if mails have been sent';
	}

	/**
	 * @return bool
	 */
	public function executeUpdate(): bool {
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($this->table);
		$queryBuilder->getRestrictions()->removeAll();
		$result = $queryBuilder->select('uid', 'sending_time')
			->from($this->table)
			->where(
				$queryBuilder->expr()->andX(
					$queryBuilder->expr()->gt('sending_time', 0),
					$queryBuilder->expr()->eq('last_sending_time', 0)
				)
			)
			->execute()->fetchAll();

		/** @var array $result */
		foreach ($result as $row) {
			$queryBuilder->update($this->table)
				->where(
					$queryBuilder->expr()->eq('uid', $queryBuilder->createNamedParameter($row['uid'], Connection::PARAM_INT))
				)
				->set('last_sending_time', (int) $row['sending_time'])
				->execute();
		}

		return TRUE;
	}

	/**
	 * @return bool
	 */
	public function updateNecessary(): bool {
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable($this->table);
		$queryBuilder->getRestrictions()->removeAll();
		$rowCount = $queryBuilder->select('*')
			->from($this->table)
			->where(
				$queryBuilder->expr()->andX(
					$queryBuilder->expr()->gt('sending_time', 0),
					$queryBuilder->expr()->eq('last_sending_time', 0)
				)
			)
			->execute()->rowCount();
		return $rowCount > 0;
	}

	/**
	 * @return array|string[]
	 */
	public function getPrerequisites(): array {
		return [
			DatabaseUpdatedPrerequisite::class
		];
	}
}
