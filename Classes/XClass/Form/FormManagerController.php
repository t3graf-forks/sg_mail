<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\XClass\Form;

use SGalinski\SgMail\Service\MailTemplateService;
use SGalinski\SgMail\Service\TypoScriptSettingsService;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\Exception;
use TYPO3\CMS\Form\Controller\FormManagerController as Typo3FormManagerController;

/**
 * Displays the form editor. Enables hooking into the save process of the form to handle automatic sg mail registration
 */
class FormManagerController extends Typo3FormManagerController {
	/**
	 * Delete the SgMail Registration file for the deleted form and clear the cache afterwards
	 *
	 * @param string $formPersistenceIdentifier persistence identifier to delete
	 * @throws Exception
	 * @throws NoSuchCacheException
	 * @internal
	 */
	public function deleteAction(string $formPersistenceIdentifier): void {
		// build the path to the register file
		$filePath = \str_replace(
			'.form.yaml',
			'.php',
			\substr($formPersistenceIdentifier, \strrpos($formPersistenceIdentifier, '/') + 1)
		);

		$fileName = GeneralUtility::getFileAbsFileName($this->getRegistrationPath() . '/' . $filePath);
		// delete the file if found
		if (\file_exists($fileName)) {
			\unlink($fileName);
			$this->clearCaches();
		}

		// needs to be done last because of the internal redirect
		parent::deleteAction($formPersistenceIdentifier);
	}

	/**
	 * Returns the path to the configured location where automatic mail template registrations should be
	 *
	 * @return string
	 * @throws Exception
	 */
	private function getRegistrationPath(): string {
		// get typoscript settings from sg mail
		$typoScriptSettingsService = GeneralUtility::makeInstance(TypoScriptSettingsService::class);

		$tsSettings = $typoScriptSettingsService->getSettings(0, 'tx_sgmail');
		// get the location where automatic registrations should be stored
		return 'EXT:' . $tsSettings['mail']['configurationLocation'] . '/' . MailTemplateService::CONFIG_PATH;
	}

	/**
	 * Clear the sgmail register cache
	 *
	 * @throws Exception
	 * @throws NoSuchCacheException
	 */
	private function clearCaches(): void {
		$cacheManager = GeneralUtility::makeInstance(CacheManager::class);

		$cache = $cacheManager->getCache(MailTemplateService::CACHE_NAME);
		$cache->flush();
	}
}
