<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\Command;

use SGalinski\SgMail\Domain\Repository\MailRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Resource\Exception\ResourceDoesNotExistException;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\Exception;

/**
 * Class DeleteOldMailsCommand
 *
 * @package SGalinski\SgMail\Command
 */
class DeleteOldMailsCommand extends Command {
	private const TABLE_NAME_MAIL = 'tx_sgmail_domain_model_mail';
	private const TABLE_NAME_FILE_REFERENCE = 'sys_file_reference';
	private const TABLE_NAME_FILE = 'sys_file';
	private const TABLE_NAME_REFINDEX = 'sys_refindex';

	/**
	 * @var int
	 */
	private $amountOfDeletedRecords = 0;

	/**
	 * Configure the command by defining the name, options and arguments
	 */
	protected function configure() {
		$this->setDescription(
			'Automatically deletes records from `tx_sgmail_domain_model_mail`, which are older than the specified age. Attachments sent as part of the email are deleted as well.'
		)->addArgument(
			'maxAge',
			InputArgument::REQUIRED,
			'The maximum age (in days), mails older than this will be deleted.'
		);
	}

	/**
	 * Executes the command for the deletion of old mails
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @return int|void|null
	 * @throws Exception
	 */
	protected function execute(InputInterface $input, OutputInterface $output) {
		$io = new SymfonyStyle($input, $output);
		$io->title($this->getDescription());
		$maxAgeArg = $input->getArgument('maxAge');
		$maxAgeInDays = (int) $maxAgeArg;

		if ($maxAgeInDays <= 0) {
			$io->error('Please enter a maximum age (in days) for the mail\'s to be deleted.');
			return 0;
		}

		$mailRepository = GeneralUtility::makeInstance(MailRepository::class);

		$mailUidsForDeletion = $mailRepository->findMailsForDeletion($maxAgeInDays);
		if (!$mailUidsForDeletion) {
			$io->success("No matching mails found for deletion criteria: older than $maxAgeInDays days");
			return 0;
		}

		$this->deleteAttachments($mailUidsForDeletion);
		$this->deleteOldMails($mailUidsForDeletion);

		if ($this->amountOfDeletedRecords > 0) {
			$io->success('Successfully deleted ' . $this->amountOfDeletedRecords . ' records.');
		} else {
			$io->note('No records deleted');
		}

		return 0;
	}

	/**
	 * Deletes all tx_sgmail_domain_model_mail records, where the uid matches one of the uids in $mailUidsForDeletion
	 *
	 * @param array $mailUidsForDeletion
	 */
	private function deleteOldMails(array $mailUidsForDeletion): void {
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(
			self::TABLE_NAME_MAIL
		);
		$this->amountOfDeletedRecords += $queryBuilder
			->delete(self::TABLE_NAME_MAIL)
			->where(
				$queryBuilder->expr()->in(
					'uid',
					$queryBuilder->createNamedParameter($mailUidsForDeletion, Connection::PARAM_INT_ARRAY)
				)
			)->execute();
	}

	/**
	 * Deletes the attachments (sys_file_reference / sys_file records), referenced in the mails to be deleted.
	 * For every file reference found in a mail record, we check if it is the only reference,
	 * or if we can find more records, referencing the same sys_file.
	 *
	 * The sys_file_reference found in the mail record is always deleted.
	 * If the referenced file has no more references, besides the one we just deleted, both the sys_file record and
	 * the actual file in the file system is deleted as well.
	 *
	 * @param array $mailUidsForDeletion
	 */
	private function deleteAttachments(array $mailUidsForDeletion): void {
		$fileReferenceQueryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(
			self::TABLE_NAME_FILE_REFERENCE
		);
		// Fetch all sys_file_reference records, with a relation to the mails we want to delete.
		$sysFileReferencesToDelete = $fileReferenceQueryBuilder
			->select('uid', 'uid_local')
			->from(self::TABLE_NAME_FILE_REFERENCE)
			->where(
				$fileReferenceQueryBuilder->expr()->in(
					'uid_foreign',
					$fileReferenceQueryBuilder->createNamedParameter(
						$mailUidsForDeletion,
						Connection::PARAM_INT_ARRAY
					)
				),
				$fileReferenceQueryBuilder->expr()->eq(
					'tablenames',
					$fileReferenceQueryBuilder->createNamedParameter(self::TABLE_NAME_MAIL, Connection::PARAM_STR)
				),
				$fileReferenceQueryBuilder->expr()->eq(
					'fieldname',
					$fileReferenceQueryBuilder->createNamedParameter('attachments', Connection::PARAM_STR)
				),
			)->execute()->fetchAll();

		foreach ($sysFileReferencesToDelete as $sysFileReferenceToDelete) {
			$sysFileReferenceUid = (int) $sysFileReferenceToDelete['uid'];
			$sysFileUid = (int) $sysFileReferenceToDelete['uid_local'];

			$resourceFactory = GeneralUtility::makeInstance(ResourceFactory::class);
			try {
				$sysFileReference = $resourceFactory->getFileReferenceObject($sysFileReferenceUid);
			} catch (ResourceDoesNotExistException $e) {
				$sysFileReference = NULL;
			}

			if ($sysFileReference === NULL) {
				return;
			}

			$referencedSysFile = $sysFileReference->getOriginalFile();

			$refIndexQueryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable(
				self::TABLE_NAME_REFINDEX
			)->createQueryBuilder();

			// Check if there are more rows in sys_refindex,
			// referencing the same file as the current $sysFileReferenceToDelete.
			// If we do not find any rows here, we delete
			// 	* the sys_file_reference record,
			// 	* the sys_file record,
			// 	* the actual file in the filesystem
			// If we find more rows here, only the sys_file_reference record is deleted.
			$countRefIndexEntries = $refIndexQueryBuilder
				->count('*')
				->from(self::TABLE_NAME_REFINDEX)
				->where(
					$refIndexQueryBuilder->expr()->eq(
						'ref_table',
						$refIndexQueryBuilder->createNamedParameter(self::TABLE_NAME_FILE, Connection::PARAM_STR)
					),
					$refIndexQueryBuilder->expr()->eq(
						'ref_uid',
						$refIndexQueryBuilder->createNamedParameter($sysFileUid, Connection::PARAM_INT)
					),
					// exclude current sys_file_reference record
					$refIndexQueryBuilder->expr()->neq(
						'recuid',
						$refIndexQueryBuilder->createNamedParameter($sysFileReferenceUid, Connection::PARAM_INT)
					)
				)->execute()->fetchOne();

			if ($countRefIndexEntries <= 0) {
				$referencedSysFile->delete();
			}

			$deleteFileReferenceQueryBuilder = GeneralUtility::makeInstance(
				ConnectionPool::class
			)->getConnectionForTable(
				self::TABLE_NAME_FILE_REFERENCE
			)->createQueryBuilder();

			// delete the sys_file_reference
			$this->amountOfDeletedRecords += $deleteFileReferenceQueryBuilder
				->delete(self::TABLE_NAME_FILE_REFERENCE)
				->where(
					$deleteFileReferenceQueryBuilder->expr()->eq(
						'uid',
						$deleteFileReferenceQueryBuilder->createNamedParameter(
							$sysFileReferenceUid,
							Connection::PARAM_INT
						)
					)
				)->execute();
		}
	}
}
