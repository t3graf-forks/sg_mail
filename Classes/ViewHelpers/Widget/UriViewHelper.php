<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgMail\ViewHelpers\Widget;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ControllerContext;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

/**
 * Class UriViewHelper
 */
class UriViewHelper extends AbstractViewHelper {
	use CompileWithRenderStatic;

	/**
	 * @var boolean
	 */
	protected $escapeOutput = FALSE;

	/**
	 * @var boolean
	 */
	protected $escapeChildren = FALSE;

	/**
	 * Initialize arguments
	 */
	public function initializeArguments(): void {
		$this->registerArgument('addQueryStringMethod', 'string', 'Method to be used for query string');
		$this->registerArgument('action', 'string', 'Target action');
		$this->registerArgument('arguments', 'array', 'Arguments', FALSE, []);
		$this->registerArgument('section', 'string', 'The anchor to be added to the URI', FALSE, '');
		$this->registerArgument('format', 'string', 'The requested format, e.g. ".html', FALSE, '');
		$this->registerArgument(
			'ajax',
			'bool',
			'TRUE if the URI should be to an AJAX widget, FALSE otherwise.',
			FALSE,
			FALSE
		);
	}

	/**
	 * @param array $arguments
	 * @param \Closure $renderChildrenClosure
	 * @param RenderingContextInterface $renderingContext
	 * @return string
	 */
	public static function renderStatic(
		array $arguments,
		\Closure $renderChildrenClosure,
		RenderingContextInterface $renderingContext
	): string {
		$ajax = $arguments['ajax'];
		if ($ajax === TRUE) {
			return static::getAjaxUri($renderingContext, $arguments);
		}
		return static::getWidgetUri($renderingContext, $arguments);
	}

	/**
	 * Get the URI for an AJAX Request.
	 *
	 * @param RenderingContextInterface $renderingContext
	 * @param array $arguments
	 * @return string the AJAX URI
	 */
	protected static function getAjaxUri(RenderingContextInterface $renderingContext, array $arguments): string {
		/** @var ControllerContext $controllerContext */
		$controllerContext = $renderingContext->getControllerContext();
		$action = $arguments['action'];
		$arguments = $arguments['arguments'];
		if ($action === NULL) {
			$action = $controllerContext->getRequest()->getControllerActionName();
		}
		$arguments['id'] = $GLOBALS['TSFE']->id;
		// @todo page type should be configurable
		$arguments['type'] = 7076;
		$arguments['fluid-widget-id'] = $controllerContext->getRequest()->getWidgetContext()->getAjaxWidgetIdentifier();
		$arguments['action'] = $action;
		return '?' . http_build_query($arguments, NULL, '&');
	}

	/**
	 * Get the URI for a non-AJAX Request.
	 *
	 * @param RenderingContextInterface $renderingContext
	 * @param array $arguments
	 * @return string the Widget URI
	 */
	protected static function getWidgetUri(RenderingContextInterface $renderingContext, array $arguments): string {
		/** @var ControllerContext $controllerContext */
		$controllerContext = $renderingContext->getControllerContext();
		$uriBuilder = $controllerContext->getUriBuilder();
		$argumentPrefix = $controllerContext->getRequest()->getArgumentPrefix();
		$parentNamespace = $controllerContext->getRequest()->getWidgetContext()->getParentPluginNamespace();
		$parentArguments = GeneralUtility::_GP($parentNamespace);
		$allArguments = [$argumentPrefix => $arguments['arguments'] ?? []];
		if ($parentArguments && isset($parentArguments['filters'])) {
			$allArguments[$parentNamespace . '[filters]'] = $parentArguments['filters'];
		}
		if ($arguments['action'] ?? FALSE) {
			$allArguments[$argumentPrefix]['action'] = $arguments['action'];
		}
		if (($arguments['format'] ?? '') !== '') {
			$allArguments[$argumentPrefix]['format'] = $arguments['format'];
		}
		return $uriBuilder->reset()
			->setArguments($allArguments)
			->setSection($arguments['section'])
			->setAddQueryString(TRUE)
			->setArgumentsToBeExcludedFromQueryString([$argumentPrefix, 'cHash'])
			->setFormat($arguments['format'])
			->build();
	}
}
