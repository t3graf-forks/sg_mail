<?php

namespace SGalinski\SgMail\ViewHelpers;

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the SG project. The SG project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * View helper to render language labels to
 * json array to be used in js applications.
 *
 * Renders to SG.lang.'extension name' object
 *
 * Example:
 * {namespace rs=SGalinski\RsEvents\ViewHelpers}
 * <rs:inlineLanguageLabels labelKeys="label01,label02" />
 */
class InlineLanguageLabelsViewHelper extends AbstractViewHelper {
	/**
	 * Specifies whether the escaping interceptors should be disabled or enabled for the render-result of this ViewHelper
	 *
	 * @see isOutputEscapingEnabled()
	 *
	 * @var boolean
	 */
	protected $escapeOutput = FALSE;

	/**
	 * Register the ViewHelper arguments
	 */
	public function initializeArguments() {
		parent::initializeArguments();
		$this->registerArgument('labelKeys', 'string', 'Comma separated list of label keys to include', FALSE, '');
		$this->registerArgument('htmlEscape', 'bool', 'Escape the output', FALSE, FALSE);
		$this->registerArgument(
			'extensionName',
			'string',
			'The extension name for the language label',
			FALSE,
			'SgComments'
		);
	}

	/**
	 * Renders the required javascript to make the language labels available
	 *
	 * @return string
	 */
	public function render(): string {
		$labelKeys = $this->arguments['labelKeys'];
		$htmlEscape = $this->arguments['htmlEscape'];
		$extensionName = $this->arguments['extensionName'];
		if (empty($extensionName)) {
			$extensionName = $this->renderingContext->getControllerContext()->getRequest()->getControllerExtensionName();
		}

		$labels = GeneralUtility::trimExplode(',', $labelKeys, TRUE);
		$languageArray = [];
		foreach ($labels as $key) {
			$value = LocalizationUtility::translate($key, $extensionName);
			$languageArray[$key] = ($htmlEscape ? \htmlentities($value) : $value);
		}

		return '
			<script type="text/javascript">
			var SG = SG || {};
			SG.lang = SG.lang || {};
			SG.lang.' . $extensionName . ' = SG.lang.' . $extensionName . ' || {};
			var languageLabels = ' . \json_encode($languageArray) . ';
			for (label in languageLabels) {
				SG.lang.' . $extensionName . '[label] = languageLabels[label];
			}
			</script>
		';
	}
}
