<?php

namespace SGalinski\SgMail\Exceptions;

use TYPO3\CMS\Core\Exception;

/**
 * Class TemplateNotFoundException
 *
 * @package SGalinski\SgMail\Exceptions
 */
class TemplateNotFoundException extends Exception {
}
