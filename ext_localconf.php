<?php

defined('TYPO3') or die();

call_user_func(
	static function () {
		// add upgrade wizard for moving all db entries to their respected site root
		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/install']['update'][\SGalinski\SgMail\Updates\UpdatePidToSiteRoot::IDENTIFIER] = \SGalinski\SgMail\Updates\UpdatePidToSiteRoot::class;
		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/install']['update'][\SGalinski\SgMail\Updates\UpdateSendingTimes::IDENTIFIER] = \SGalinski\SgMail\Updates\UpdateSendingTimes::class;
		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/install']['update'][\SGalinski\SgMail\Updates\MigrateSchedulerTasks::IDENTIFIER] = \SGalinski\SgMail\Updates\MigrateSchedulerTasks::class;
		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/install']['update'][\SGalinski\SgMail\Updates\MigrateFinishersUpgrade::IDENTIFIER] = \SGalinski\SgMail\Updates\MigrateFinishersUpgrade::class;
		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/install']['update'][\SGalinski\SgMail\Updates\SendStatusUpdate::IDENTIFIER] = \SGalinski\SgMail\Updates\SendStatusUpdate::class;
		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/install']['update'][\SGalinski\SgMail\Updates\LanguageMigrationUpdate::IDENTIFIER] = \SGalinski\SgMail\Updates\LanguageMigrationUpdate::class;

		\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptSetup(
			'@import "EXT:sg_mail/Configuration/TypoScript/setup.typoscript"'
		);

		$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Form\Controller\FormManagerController::class] = [
			'className' => \SGalinski\SgMail\XClass\Form\FormManagerController::class,
		];

		// Datamap process hook
		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'][] = \SGalinski\SgMail\Hooks\ProcessDatamap::class;
		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/form']['beforeFormSave'][] = \SGalinski\SgMail\Hooks\FormEditorBeforeFormSave::class;

		// Cache registration
		$registerService = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
			\SGalinski\SgMail\Service\RegisterService::class
		);
		$registerService->registerCache();
	}
);
