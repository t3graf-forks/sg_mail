## Version 8 Breaking Changes

- Dropped TYPO3 9 Support
- Dropped php 7.3 Support
- Replaced ```BeforeSendingMail``` Hook with the Event ```BeforeSendingFormsMailEvent```
  (Please look into the README to see an example for integrating the event.)

## Version 7 Breaking Changes

- Using and requiring the actual SiteLanguage everywhere instead of perky ISO codes
- Require the setting of a pid when using the MailTemplateService to not have templates and mails float everywhere uncontrollably
- MailTemplateService is a Singleton instance now to stop loading objects en mass when instanciating it
- deprecated preview parameter in MailTemplateService::sendEmail method, because ignoreMailQueue is used instead

## Version 6 Breaking Changes

- Dropped TYPO3 8 support
- set ignoreMailQueue to TRUE by default
