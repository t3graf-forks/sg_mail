<?php

defined('TYPO3') or die();

call_user_func(
	static function () {
		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks'][\TYPO3\CMS\Scheduler\Task\TableGarbageCollectionTask::class]['options']['tables']['tx_sgmail_domain_model_mail'] = [
			'dateField' => 'sending_time',
			'expirePeriod' => 180
		];

		\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
			'SgMail',
			'web',
			'Mail',
			'',
			[
				\SGalinski\SgMail\Controller\MailController::class => 'index, sendTestMail, empty, reset',
				\SGalinski\SgMail\Controller\QueueController::class => 'index, sendMail, export, preview',
				\SGalinski\SgMail\Controller\ConfigurationController::class => 'index, create, edit, delete',
				\SGalinski\SgMail\Controller\LayoutController::class => 'index',
				\SGalinski\SgMail\Controller\NewsletterController::class => 'index, sendTestMail, empty',
				\SGalinski\SgMail\Controller\SiteController::class => 'index'
			],
			[
				'access' => 'user,group',
				'icon' => 'EXT:sg_mail/Resources/Public/Icons/ModuleIcon.svg',
				'labels' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang.xlf',
			]
		);

		\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_sgmail_domain_model_mail');
		\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages(
			'tx_sgmail_domain_model_template'
		);
		\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_sgmail_domain_model_layout');
	}
);
