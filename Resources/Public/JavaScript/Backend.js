/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

define([
	'jquery',
	'TYPO3/CMS/Backend/ModuleMenu',
	'TYPO3/CMS/Backend/Modal',
	'TYPO3/CMS/Backend/Viewport',
	'TYPO3/CMS/Core/Ajax/AjaxRequest'
], function($, ModuleMenu, Modal, Viewport, AjaxRequest) {
	'use strict';

	var SgMail = {
		resetTemplateListener: function(_event) {
			_event.preventDefault();

			var confirm = TYPO3.lang['backend.delete_template'];
			if (window.confirm(confirm)) {
				window.location = $(_event.currentTarget).attr('href');
			}
		},
		deleteTemplateListener: function(_event) {
			_event.preventDefault();

			var confirm = TYPO3.lang['backend.delete_template_custom'];
			if (window.confirm(confirm)) {
				window.location = $(_event.currentTarget).attr('href');
			}
		},
		sendMailListener: function(_event) {
			_event.preventDefault();

			var confirm = TYPO3.lang['backend.send_mail_manually'];
			if (window.confirm(confirm)) {
				window.location = $(_event.currentTarget).attr('href');
			}
		},
		resendMailListener: function(_event) {
			_event.preventDefault();

			var confirm = TYPO3.lang['backend.send_mail_again'];
			if (window.confirm(confirm)) {
				window.location = $(_event.currentTarget).attr('href');
			}
		},
		toggleMailBody: function(_event) {
			var uid = $(_event.currentTarget).data('uid');
			$('#toggle-' + uid).detach().appendTo('body').modal('show');
		},
		init: function() {
			$(document).ready(function() {
				$('.reset-btn').on('click', SgMail.resetTemplateListener);
				$('#delete-template-btn').on('click', SgMail.deleteTemplateListener);
				$('.btn-send-now').on('click', SgMail.sendMailListener);
				$('.btn-resend').on('click', SgMail.resendMailListener);
				$('.btn-toggle').on('click', SgMail.toggleMailBody);
				$('#filter-reset-btn').on('click', function(event) {
					event.preventDefault();
					this.form.reset();
					$(this).closest('form').find('select option').each(function() {
						$(this).removeAttr('selected');
					});

					$(this).closest('form').find('input[type="checkbox"]').each(function() {
						$(this).removeAttr('checked');
					});

					$('.reset-me').val('');
					$('#filters-all').attr('checked', true);

					this.form.submit();
				});
				$('.sg-mail_pageswitch').on('click', function(event) {
					if(Viewport.NavigationContainer.PageTree !== undefined) {
						event.preventDefault();
						SgMail.goTo('web_SgMailMail', event.target.dataset.page, event.target.dataset.path);
					}
				});
				$('.btn-preview').on('click', function(event) {
					event.preventDefault();
					if(!$(event.target).hasClass('btn-preview')) {
						var element = $(event.target).parents('.btn-preview')[0];
					}
					else {
						var element = event.target;
					}
					SgMail.openMailPreview(element.dataset.href);
				})
			});
		},
		jumpExt: function(URL, anchor) {
			var anc = anchor ? anchor : "";
			window.location.href = URL + (T3_THIS_LOCATION ? "&returnUrl=" + T3_THIS_LOCATION : "") + anc;
			return false;
		},
		jumpSelf: function(URL) {
			window.location.href = URL + (T3_RETURN_URL ? "&returnUrl=" + T3_RETURN_URL : "");
			return false;
		},
		jumpToUrl: function(URL) {
			window.location.href = URL;
			return false;
		},
		setHighlight: function(id) {
			top.fsMod.recentIds["web"] = id;
			top.fsMod.navFrameHighlightedID["web"] = "pages" + id + "_" + top.fsMod.currentBank;    // For highlighting
			if (top.content && top.content.nav_frame && top.content.nav_frame.refresh_nav) {
				top.content.nav_frame.refresh_nav();
			}
		},
		goTo: function(module, id) {
			var pageTreeNodes = Viewport.NavigationContainer.PageTree.instance.nodes;
			for (var nodeIndex in pageTreeNodes) {
				if (pageTreeNodes.hasOwnProperty(nodeIndex) && pageTreeNodes[nodeIndex].identifier === parseInt(id)) {
					Viewport.NavigationContainer.PageTree.selectNode(pageTreeNodes[nodeIndex]);
					break;
				}
			}
			ModuleMenu.App.showModule(module, 'id=' + id);
		},
		openMailPreview: function(url) {
			var iframe = $('<iframe src="'+url+'" width="100%" height="500px;"></iframe>');
			Modal.confirm('Preview', iframe, 'info');
		}
	};

	TYPO3.SgMail = SgMail;

	SgMail.init();

	if (typeof window.jumpExt === 'undefined') {
		window.jumpExt = SgMail.jumpExt;
	}

	if (typeof window.jumpSelf === 'undefined') {
		window.jumpSelf = SgMail.jumpSelf;
	}

	if (typeof window.jumpToUrl === 'undefined') {
		window.jumpToUrl = SgMail.jumpToUrl;
	}

	return SgMail;
});
