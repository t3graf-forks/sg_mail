/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

define([
	'jquery',
	'TYPO3/CMS/Backend/ModuleMenu',
	'TYPO3/CMS/Backend/Viewport'
], function($, ModuleMenu, Viewport) {
	'use strict';

	var Newsletter = {
		init: function() {
			if (selectedGroupsData) {
				$(document.getElementById("parameters-selectedGroups")).select2ToTree({
					treeData: {dataArr: selectedGroupsData},
					valFld: 'uid',
					labelFld: 'title'
				});
			}

			$('#newsletter-send-real-emails-button').on('click', function () {
				var confirmMessage = SG.lang.SgMail['backend.newsletter.confirm_send'];
				if (window.confirm(confirmMessage)) {
					$('#sgMail-newsletter-form > div.form-group input[type=submit]').each(function (index, element) {
						$(element).prop('disabled', true);
					})
					$('#newsletter-send-real-emails-hidden-field').val("1");
					$('#sgMail-newsletter-form').trigger('submit');
					return true;
				} else {
					return false;
				}

			});

			$('#newsletter-send-preview-emails-button').on('click', function () {
				$('#sgMail-newsletter-form > div.form-group input[type=submit]').each(function (index, element) {
					$(element).prop('disabled', true);
				})
				$('#newsletter-send-real-emails-hidden-field').val("0");
				$('#sgMail-newsletter-form').trigger('submit');
				return true;
			});
		}
	}

	TYPO3.Newsletter = Newsletter;
	Newsletter.init();

	return Newsletter;
});
