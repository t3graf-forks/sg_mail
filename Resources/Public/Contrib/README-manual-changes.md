## select2-to-tree

### JavaScript
I changed the buildSelect() function so that it correctly reads
the labelFld, valFld, selFld option

### CSS
.s2-to-tree li.select2-results__option.non-leaf .expand-collapse:before -
removed the top: directive

## select2.full.js
- I changed MultipleSelection.prototype.update() so that if we have
clone groups (groups with more than one parent), it only adds one token
with that value
- I changed SelectAdapter.prototype.unselect() so that if you unselect one
of the clone groups (groups with more than one parent), it unselects all
