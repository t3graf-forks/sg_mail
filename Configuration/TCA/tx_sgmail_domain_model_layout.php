<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c] sgalinski Internet Services (https://www.sgalinski.de]
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option] any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

$columns = [
	'ctrl' => [
		'title' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_layout',
		'label' => 'name',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'searchFields' => 'name, content',
		'delete' => 'deleted',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'translationSource' => 'l10n_source',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'enablecolumns' => [
			'disabled' => 'hidden',
		],
		'default_sortby' => 'name',
		'hideTable' => TRUE,
		'iconfile' => 'EXT:sg_mail/Resources/Public/Icons/ModuleIconTCA.svg'
	],
	'interface' => [],
	'types' => [
		'1' => [
			'showitem' => 'hidden,--palette--;;name,head_content,content'
		],
	],
	'palettes' => [
		'name' => [
			'showitem' => 'name, default'
		],
	],
	'columns' => [
		'hidden' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:hidden.I.0',
			'config' => [
				'type' => 'check',
			],
		],
		'sys_language_uid' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectSingle',
				'special' => 'languages',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => [
					[
						'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
						-1,
						'flags-multiple'
					]
				],
				'default' => 0,
				'fieldWizard' => [
					'selectIcons' => [
						'disabled' => FALSE
					]
				]
			]
		],
		'l10n_parent' => [
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => [
					['', 0]
				],
				'foreign_table' => 'tx_sgmail_domain_model_layout',
				'foreign_table_where' => 'AND tx_sgmail_domain_model_layout.uid=###REC_FIELD_l10n_parent### AND tx_sgmail_domain_model_layout.sys_language_uid IN (-1,0)',
				'default' => 0
			]
		],
		'l10n_diffsource' => [
			'config' => [
				'type' => 'passthrough',
				'default' => ''
			]
		],
		'default' => [
			'exclude' => TRUE,
			'l10n_mode' => 'exclude',
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_layout.default',
			'config' => [
				'type' => 'check',
			],
		],
		'name' => [
			'exclude' => TRUE,
			'l10n_display' => 'defaultAsReadonly',
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_layout.name',
			'config' => [
				'type' => 'input',
				'eval' => 'required, trim'
			],
		],
		'content' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_layout.content',
			'description' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_layout.content.info',
			'config' => [
				'type' => 'text',
				'renderType' => 't3editor'
			],
		],
		'head_content' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_layout.head_content',
			'description' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_layout.head_content.info',
			'config' => [
				'type' => 'text',
				'renderType' => 't3editor'
			],
		],
	]
];

return $columns;
