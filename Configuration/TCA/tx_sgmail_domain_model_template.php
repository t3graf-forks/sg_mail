<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c] sgalinski Internet Services (https://www.sgalinski.de]
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option] any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

$columns = [
	'ctrl' => [
		'title' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_template',
		'label' => 'extension_key',
		'label_alt' => 'template_name',
		'label_alt_force' => TRUE,
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'searchFields' => 'extension_key, template_name, language, subject, from_name, from_mail, reply_to, to_address',
		'delete' => 'deleted',
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l18n_parent',
		'translationSource' => 'l10n_source',
		'transOrigDiffSourceField' => 'l18n_diffsource',
		'enablecolumns' => [
			'disabled' => 'hidden',
		],
		'default_sortby' => 'ORDER BY extension_key ASC, template_name ASC',
		'iconfile' => 'EXT:sg_mail/Resources/Public/Icons/ModuleIconTCA.svg',
	],
	'interface' => [],
	'types' => [
		'1' => [
			'showitem' => 'hidden,--palette--;;1,sys_language_uid,extension_key,layout,template_name,subject,from_name,from_mail,to_address,content,cc,bcc,reply_to'
		],
	],
	'columns' => [
		'sys_language_uid' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectSingle',
				'special' => 'languages'
			]
		],
		'hidden' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:hidden.I.0',
			'config' => [
				'type' => 'check',
			],
		],
		'layout' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_template.layout',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => [
					['', 0]
				],
				'foreign_table' => 'tx_sgmail_domain_model_layout',
				'foreign_table_where' => 'AND tx_sgmail_domain_model_layout.pid=###CURRENT_PID### AND tx_sgmail_domain_model_layout.sys_language_uid IN (-1,0)',
				'default' => 0
			],
		],
		'extension_key' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_template.extension_key',
			'config' => [
				'type' => 'input',
				'eval' => 'required, trim'
			],
		],
		'content' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_template.content',
			'config' => [
				'type' => 'text',
				'cols' => 40,
				'rows' => 10,
			],
		],
		'template_name' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_template.template_name',
			'config' => [
				'type' => 'input',
				'eval' => 'required, trim'
			],
		],
		'subject' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_template.subject',
			'config' => [
				'type' => 'input',
				'eval' => 'required'
			],
		],
		'from_name' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_template.from_name',
			'config' => [
				'type' => 'input'
			],
		],
		'from_mail' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_template.from_mail',
			'config' => [
				'type' => 'input'
			],
		],
		'cc' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_template.cc',
			'config' => [
				'type' => 'input'
			],
		],
		'bcc' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_template.bcc',
			'config' => [
				'type' => 'input'
			],
		],
		'reply_to' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_template.reply_to',
			'config' => [
				'type' => 'input'
			],
		],
		'to_address' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_template.to_address',
			'config' => [
				'type' => 'input'
			],
		],
		'default_attachments' => [
			'exclude' => TRUE,
			'label' => 'LLL:EXT:sg_mail/Resources/Private/Language/locallang_db.xlf:tx_sgmail_domain_model_template.default_attachments',
			'config' => [
				'type' => 'text',
				'cols' => 40,
				'rows' => 10
			]
		]
	]
];

return $columns;
